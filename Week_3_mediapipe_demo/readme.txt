startup-command for webcam:     [file_location_of_python_file]\Week_3_mediapipe_demo.py -m 1
startup-command for file input: [file_location_of_python_file]\Week_3_mediapipe_demo.py -m 2 -i [file_location_of_input_data]

used libraries:
    -cv2
    -mediapipe
    -numpy
    -argparse
    -flask
    -sympy

The build_docker batch-file will create a docker image of the repository
The run_docker batch-file will start a docker container from the image on localhost, port 5000
However, the pose recognition runs on localhost port 2204 (localhost:2204/video_feed)