echo >====================Installing libraries=========================
python-3.9 -m pip install torch
python-3.9 -m pip install torchvision
python-3.9 -m pip install numpy
python-3.9 -m pip install cv2
echo >====================Running demo=================================
python-3.9 'Week_3_mediapipe_demo\src\Week_3_mediapipe_demo.py'