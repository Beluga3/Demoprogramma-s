from re import M
import numpy as np
import mediapipe as mp
#import argparse
import cv2
from flask import Flask, Response
from sympy import threaded

#parser = argparse.ArgumentParser()
#parser.add_argument('-m', '--mode', required=True,
#                    help='selected mode (1 for webcam, 2 for file)')
#parser.add_argument('-i', '--input-location', required=False,
#                    help='path to the input data')
#args = vars(parser.parse_args())

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

app = Flask(__name__)

@app.route('/')
def index():
    return "Default message"

def gen():
    capture = cv2.VideoCapture(0)
    with mp_pose.Pose(
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as pose:
        while capture.isOpened():
            success, image = capture.read()
            if not success:
                print("Ignoring empty camera frame")
                continue
            image.flags.writeable = False
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            results = pose.process(image)

            #draw the pose annotation on the image
            image.flags.writeable = True
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            mp_drawing.draw_landmarks(
                image,
                results.pose_landmarks,
                mp_pose.POSE_CONNECTIONS,
                landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())

            #flip the image horizontally
            cv2.imshow('MediaPipe Pose', cv2.flip(image, 1))
            if cv2.waitKey(5) & 0xFF == 27:
                break
    capture.release()

#locatie instellen voor portal
@app.route('/video_feed')
def video_feed():
    global video
    return Response(gen(),
    mimetype='multipart/x-mixed-replace;boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=2204, threaded=True)
