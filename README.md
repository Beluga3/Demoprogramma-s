# Demoprogramma-s
PyTorch / PyVision project:
  startup-command for images: '[src_folder_location]\keypoint_rcnn_images.py' -i [file_location_of_input_data]
  startup-command for videos: '[src_folder_location]\keypoint_rcnn_videos.py' -i [file_location_of_input_data]

  used libraries:
     -torch
    -torchvision
    -numpy
    -cv2
    -argparse
    -time

#MediaPipe project:
  startup-command for webcam:     [file_location_of_python_file]\Week_3_mediapipe_demo.py -m 1
  startup-command for file input: [file_location_of_python_file]\Week_3_mediapipe_demo.py -m 2 -i [file_location_of_input_data]

  used libraries:
    -cv2
    -mediapipe
    -numpy
    -argparse
