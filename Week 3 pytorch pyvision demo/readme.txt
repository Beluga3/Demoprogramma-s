startup-command for images: '[src_folder_location]\keypoint_rcnn_images.py' -i [file_location_of_input_data]
startup-command for videos: '[src_folder_location]\keypoint_rcnn_videos.py' -i [file_location_of_input_data]

used libraries:
    -torch
    -torchvision
    -numpy
    -cv2
    -argparse
    -time