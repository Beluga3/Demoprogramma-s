import torch
import torchvision
import cv2
import argparse
import utils
import time

from PIL import Image
from torchvision.transforms import transforms as transforms

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, 
                    help='path to the input data')
args = vars(parser.parse_args())
# transform to convert the image to tensor
transform = transforms.Compose([
    transforms.ToTensor()
])
# initialize the model
model = torchvision.models.detection.keypointrcnn_resnet50_fpn(pretrained=True,
                                                               num_keypoints=17)
# set the computation device
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# load the modle on to the computation device and set to eval mode
model.to(device).eval()

cap = cv2.VideoCapture(args['input'])
if (cap.isOpened() == False):
    print("Fout bij het lezen van de video. Controleer het pad.")
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))

save_path = f"../outputs/{args['input'].split('/')[-1].split(' ')[0]}.mp4"
out = cv2.VideoWriter(save_path,
                      cv2.VideoWriter_fourcc(*'mp4v'), 20,
                      (frame_width, frame_height))
frame_count = 0
total_fps = 0

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret == True:
        pil_image = Image.fromarray(frame).convert('RGB')
        orig_frame = frame
        #transform the image
        image = transform(pil_image)
        #add a batch dimension
        image = image.unsqueeze(0).to(device)
        #get start time
        start_time = time.time()
        with torch.no_grad():
            outputs = model(image)
        #get end time
        end_time = time.time()
        output_image = utils.draw_keypoints(outputs, orig_frame)

        #get the fps
        fps = 1 / (end_time - start_time)
        #add fps to total fps
        total_fps += fps
        frame_count += 1
        wait_time = max(1, int(fps/4))

        cv2.imshow('Pose detection frame', output_image)
        out.write(output_image)
        #press q to exit
        if cv2.waitKey(wait_time) & 0xFF == ord('q'):
            break
    else:
        break

#release VideoCapture()
cap.release()
cv2.destroyAllWindows()
avg_fps = total_fps / frame_count
print(f"Average FPS: {avg_fps:.3f}")
